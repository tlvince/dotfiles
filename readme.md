# dotfiles

## Usage

```bash
stow --simulate --verbose=3 \
  --ignore="^.git(modules|ignore)$|readme.m(d|kd)|.DS_Store|init.sh" \
  package ...
```

## Author

© 2013 Tom Vincent <http://tlvince.com/contact>

## License

Released under the [MIT License](http://tlvince.mit-license.org).
